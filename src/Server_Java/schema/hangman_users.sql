create table users
(
    id                 int auto_increment
        primary key,
    username           varchar(50) null,
    password           varchar(50) null,
    remaining_attempts int         null,
    constraint users_username_uindex
        unique (username)
);

INSERT INTO hangman.users (id, username, password, remaining_attempts) VALUES (1, 'root', 'tree', 5);
INSERT INTO hangman.users (id, username, password, remaining_attempts) VALUES (2, 'tree', 'root', 5);
INSERT INTO hangman.users (id, username, password, remaining_attempts) VALUES (3, 'bayag', 'hatdog', 5);
