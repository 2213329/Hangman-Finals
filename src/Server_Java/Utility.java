package Server_Java;

import exc.InvalidPasswordException;
import exc.UserNotFoundException;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

public class Utility {
    /* Register JDBC Driver
    // change the word mariadb to mysql
    String dbConfig = "jdbc:mariadb://127.0.0.1:3306" + "/"
            + database + "?"
            + "use  r=" + username + "&"
            + "password=";
 */
    private Connection connection;
    private List<String> wordSet;
    private String dbConfig = "jdbc:mariadb://127.0.0.1:3306" + "/"
            + "hangman" + "?"
            + "user=" + "root" + "&"
            + "password=";

    public Utility() {
        connect(dbConfig);
        wordSet = populateWordSet();
        try {
            // Dev environment
            Class.forName("org.mariadb.jdbc.Driver");
            // Use this for a MySQL Server
            // Class.forName("com.mysql.jdbc.Driver")
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void connect(String configString) {
        try {
            connection = DriverManager.getConnection(configString);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void importWordSet(List<String> wordSet) {
        PreparedStatement preparedStatement;
        String query = "INSERT INTO words VALUES(NULL, ?)";
        try {
            preparedStatement = connection.prepareStatement(query);
            for (String word : wordSet) {
                preparedStatement.setString(1, word);
                preparedStatement.execute();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public List<String> populateWordSet() {
        Statement statement;
        ResultSet resultSet;
        List<String> wordSet = new ArrayList<>();
        String query = "SELECT word FROM words";
        try {
            statement = connection.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
                    ResultSet.CONCUR_UPDATABLE);
            resultSet = statement.executeQuery(query);
            // move marker to the beginning
            resultSet.beforeFirst();
            while (resultSet.next()) {
                wordSet.add(resultSet.getString(1));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return wordSet;
    }

    public String getGuessWord() {
        int randomIndex = ThreadLocalRandom.current().nextInt(0, 2288 + 1);
        return wordSet.get(randomIndex);
    }

    public void login(String username, String password) throws UserNotFoundException, InvalidPasswordException {
        PreparedStatement preparedStatement;
        ResultSet resultSet;
        String query = "SELECT username, password FROM users WHERE username=?";
        try {
            preparedStatement = connection.prepareStatement(query);
            preparedStatement.setString(1, username);
            resultSet = preparedStatement.executeQuery();
            // check for the size of the resultset
            if (!resultSet.isBeforeFirst()) throw new UserNotFoundException("Invalid username.");
            // navigate to the beginning of the resultset
            resultSet.first();
            if (!resultSet.getString(2).equals(password))
                throw new InvalidPasswordException("Invalid password.");

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public int getRemainingAttempts(String username) {
        PreparedStatement preparedStatement;
        ResultSet resultSet;
        String query = "SELECT remaining_attempts FROM users WHERE username=?";

        try {
            preparedStatement = connection.prepareStatement(query);
            preparedStatement.setString(1, username);
            resultSet = preparedStatement.executeQuery();
            resultSet.first();
            return resultSet.getInt(1);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return -1;
    }

    public void decrementRemainingAttempts(String username) {
        PreparedStatement preparedStatement;
        ResultSet resultSet;
        int tempInt = 0;
        String query = "SELECT id, remaining_attempts FROM users WHERE username=?";

        try {
            preparedStatement = connection.prepareStatement(query, ResultSet.TYPE_FORWARD_ONLY,
                    ResultSet.CONCUR_UPDATABLE);
            preparedStatement.setString(1, username);
            resultSet = preparedStatement.executeQuery();
            resultSet.first();
            tempInt = resultSet.getInt(2);
            resultSet.updateInt(2, --tempInt);
            resultSet.updateRow();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void resetAttempts(String username) {
        PreparedStatement preparedStatement;
        ResultSet resultSet;
        String query = "SELECT id, remaining_attempts FROM users WHERE username=?";

        try {
            preparedStatement = connection.prepareStatement(query, ResultSet.TYPE_FORWARD_ONLY,
                    ResultSet.CONCUR_UPDATABLE);
            preparedStatement.setString(1, username);
            resultSet = preparedStatement.executeQuery();
            resultSet.first();
            resultSet.updateInt(2, 0);
            resultSet.updateRow();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
