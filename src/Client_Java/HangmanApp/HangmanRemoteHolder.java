package Client_Java.HangmanApp;

/**
* HangmanApp/HangmanRemoteHolder.java .
* Generated by the IDL-to-Java compiler (portable), version "3.2"
* from Hangman_remote.idl
* Monday, May 2, 2022 9:13:52 PM CST
*/

public final class HangmanRemoteHolder implements org.omg.CORBA.portable.Streamable
{
  public HangmanRemote value = null;

  public HangmanRemoteHolder ()
  {
  }

  public HangmanRemoteHolder (HangmanRemote initialValue)
  {
    value = initialValue;
  }

  public void _read (org.omg.CORBA.portable.InputStream i)
  {
    value = HangmanRemoteHelper.read (i);
  }

  public void _write (org.omg.CORBA.portable.OutputStream o)
  {
    HangmanRemoteHelper.write (o, value);
  }

  public org.omg.CORBA.TypeCode _type ()
  {
    return HangmanRemoteHelper.type ();
  }

}
